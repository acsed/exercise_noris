package com.noris.exercise_noris.model.dto;

import lombok.Data;

@Data
public class PhoneDTO {
    private String number;
    private int citycode;
    private String contrycode;
}
