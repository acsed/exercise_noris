package com.noris.exercise_noris.model.dto;

import lombok.Data;

import java.util.Date;

@Data
public class UserResponseDTO {

    private Long id;
    private String name;
    private String email;
    private Date created;
    private Date modified;
    private Date lastLogin;
    private boolean isActive;


}
