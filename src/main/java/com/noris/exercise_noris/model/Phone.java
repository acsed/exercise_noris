package com.noris.exercise_noris.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="phones")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Phone implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id",  insertable = false, updatable = false)
    private User user;

    private String number;
    private int  citycode;
    private String contrycode;
}
