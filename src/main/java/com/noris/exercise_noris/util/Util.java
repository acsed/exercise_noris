package com.noris.exercise_noris.util;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class Util {

    public static boolean validarFormato(String cadena) {
        // Expresión regular para validar el formato
        String regex = "^(?=.*[A-Z])(?=.*\\d.*\\d)(?!.*\\d.*\\d.*\\d)(?!.*[A-Z].*[A-Z]).{8,12}$";

        // Compilar la expresión regular
        Pattern pattern = Pattern.compile(regex);

        // Crear un objeto Matcher
        Matcher matcher = pattern.matcher(cadena);

        // Comprobar si la cadena coincide con el formato
        return matcher.matches();
    }

    public static void main(String[] args) {
        String cadena1 = "a2asfGfdfdf4"; // Cumple con el formato
        String cadena2 = "Aa1b2C3"; // No cumple con el formato
        String cadena3 = "Ab1"; // No cumple con la longitud mínima

        System.out.println("Cadena 1 cumple con el formato: " + validarFormato(cadena1));
        System.out.println("Cadena 2 cumple con el formato: " + validarFormato(cadena2));
        System.out.println("Cadena 3 cumple con el formato: " + validarFormato(cadena3));
    }
}
