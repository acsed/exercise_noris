package com.noris.exercise_noris;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExerciseNorisApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExerciseNorisApplication.class, args);
	}

}
