package com.noris.exercise_noris.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ValidationConfig {

    @Value("${user.name.validation.regex}")
    private String userNameRegex;

    @Value("${user.password.validation.regex}")
    private String passwordRegex;

    // Getters y setters

    public String getUserNameRegex() {
        return userNameRegex;
    }

    public void setUserNameRegex(String userNameRegex) {
        this.userNameRegex = userNameRegex;
    }

    public String getPasswordRegex() {
        return passwordRegex;
    }

    public void setPasswordRegex(String passwordRegex) {
        this.passwordRegex = passwordRegex;
    }

}
