package com.noris.exercise_noris.controller;

import com.noris.exercise_noris.config.ValidationConfig;
import com.noris.exercise_noris.model.Phone;
import com.noris.exercise_noris.model.User;
import com.noris.exercise_noris.model.dto.PhoneDTO;
import com.noris.exercise_noris.model.dto.UserRequestDTO;
import com.noris.exercise_noris.model.dto.UserResponseDTO;
import com.noris.exercise_noris.service.UserService;
import com.noris.exercise_noris.util.Util;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/user/")
@Api(tags = "User Controller", description = "Operaciones relacionadas con usuarios")
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    private ValidationConfig validationConfig;

    String errorMessage ;

    @PostMapping("/save")
    @ApiOperation(value = "Crear un nuevo usuario", notes = "Operaciones relacionadas con usuarios.")
    @ApiImplicitParams({@ApiImplicitParam(name = "userRequest", value = "Información del usuario a crear", dataType = "UserRequestDTO",
            example = "{\n  \"name\": \"Juan Rodriguez\",\n  \"email\": \"juan@rodriguez.org\",\n  \"password\": \"Hunte@r22\",\n  \"phones\": [\n    {\n      \"number\": 1234567,\n      \"citycode\": 1,\n      \"contrycode\": \"57\"\n    }\n  ]\n}")
    })
            public ResponseEntity<?> createUser(@RequestBody UserRequestDTO userRequest) {
        try {
            String errorMessage = "Error al crear el usuario";
            boolean isValidMail = EmailValidator.getInstance().isValid(userRequest.getEmail());



            if (isValidMail == false) {
                // email incorreto
                errorMessage = "Usuario con el correo electronico no valido";
                return new ResponseEntity<>(formatErrorResponse(errorMessage), HttpStatus.CONFLICT);
            }

            if (isValidMail == false) {
                // email incorreto
                errorMessage = "Usuario con el correo electronico no valido";
                return new ResponseEntity<>(formatErrorResponse(errorMessage), HttpStatus.CONFLICT);
            }


            // Validación de la contraseña
            if (!validatePassword(userRequest.getPassword())) {
                errorMessage = "El formato de la contraseña no es válido. (regular para validar contraseñas que contienen al menos 8 caracteres, al menos una letra minúscula, al menos una letra mayúscula, al menos un número y al menos un carácter especial (por ejemplo, !, @, #, $, %, ^, & o *):)";
                return new ResponseEntity<>(formatErrorResponse(errorMessage), HttpStatus.BAD_REQUEST);
            }

            // consultas bd

            if (userService.existsUserByEmail(userRequest.getEmail())) {
                // Duplicate mail detected
                errorMessage = "Correo ingresado ya existe !";
                return new ResponseEntity<>(formatErrorResponse(errorMessage), HttpStatus.CONFLICT);
            }

            //PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            //String encodedPassword = passwordEncoder.encode(userRequest.getPassword());

            //userRequest.setPassword(encodedPassword);

            // Convierte el DTO a una entidad User
            User newUser = convertToUserEntity(userRequest);

            // Guarda el nuevo usuario

            User savedUser = userService.save(newUser);

            UserResponseDTO responseDTO = convertToUserResponse(savedUser);

            return new ResponseEntity<>(responseDTO, HttpStatus.CREATED);
        } catch (Exception e) {
            errorMessage = "Error al crear el usuario";
            return new ResponseEntity<>(formatErrorResponse(errorMessage), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private User convertToUserEntity(UserRequestDTO userRequest) {
        User user = new User();
        user.setName(userRequest.getName());
        user.setEmail(userRequest.getEmail());
        user.setPassword(userRequest.getPassword());

        List<Phone> phones = userRequest.getPhones().stream()
                .map(this::convertToPhoneEntity)
                .collect(Collectors.toList());

        user.setPhones(phones);

        return user;
    }

    private Phone convertToPhoneEntity(PhoneDTO phoneDTO) {
        Phone phone = new Phone();
        phone.setNumber(phoneDTO.getNumber());
        phone.setCitycode(phoneDTO.getCitycode());
        phone.setContrycode(phoneDTO.getContrycode());

        return phone;
    }

    private UserResponseDTO convertToUserResponse(User user) {
        UserResponseDTO responseDTO = new UserResponseDTO();
        responseDTO.setId(user.getId());
        responseDTO.setName(user.getName());
        responseDTO.setEmail(user.getEmail());
        responseDTO.setCreated(user.getCreate());
        responseDTO.setModified(user.getLastLogin());
        responseDTO.setLastLogin(user.getLastLogin());
        responseDTO.setActive(user.isActive());

        return responseDTO;
    }

    private Map<String, String> formatErrorResponse(String errorMessage) {
        Map<String, String> errorResponse = new HashMap<>();
        errorResponse.put("mensaje", errorMessage);
        return errorResponse;
    }

    private boolean validatePassword(String password) {
        String regex = validationConfig.getPasswordRegex();
        return password.matches(regex);
    }


}
