package com.noris.exercise_noris.service;

import com.noris.exercise_noris.model.User;
import com.noris.exercise_noris.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;


    public User save(User user) {
        return userRepository.save(user);
    }

    public boolean existsUserByEmail(String email) {
        return userRepository.existsByEmail(email);
    }



}
