# exercise_noris

## Descripción
`exercise_noris` es una aplicación Spring Boot que utiliza Java 1.8, Spring Boot 2.5.7, JPA con Hibernate y una base de datos H2.

## Requisitos Previos
- Java Development Kit (JDK) 1.8 o superior: [Descargar JDK](https://www.oracle.com/java/technologies/javase-downloads.html)
- Maven: [Descargar Maven](https://maven.apache.org/download.cgi)

## Configuración del Proyecto

### Base de Datos
La aplicación utiliza una base de datos H2. Puedes acceder a la consola de la base de datos en `http://localhost:puerto/h2-console`. Las credenciales por defecto son `username: sa` y `password: password`.

### Configuración de la Aplicación
- La configuración principal se encuentra en `src/main/resources/application.properties`.

## Ejecución de la Aplicación
Para ejecutar la aplicación, puedes usar el siguiente comando Maven:

```bash
mvn spring-boot:run